@component('mail::message')
    Новая заявка в обратной связи {{$model->created_at}}
    @if($model->name)
        Имя: {{ucfirst($model->name)}}
    @endif
    @if($model->name)
        Контактный телефон: {{ucfirst($model->phone)}}
    @endif
    @if($model->email)
        Контактный email: {{ucfirst($model->email)}}
    @endif

    @component('mail::button', ['url' => $url])
        Перейти к заявке
    @endcomponent
