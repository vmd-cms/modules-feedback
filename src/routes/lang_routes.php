<?php
use Illuminate\Support\Facades\Route;

return function (){
    Route::middleware([
        'throttle:30,1',
    ])->group(function (){
        Route::post('/feedback/{key}', 'App\Modules\Feedback\Controllers\FeedbackController@store');
    });
};
