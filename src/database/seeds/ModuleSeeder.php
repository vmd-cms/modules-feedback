<?php
namespace VmdCms\Modules\Feedback\database\seeds;

use App\Modules\Feedback\Sections\Components\FeedbackGroupBlock;
use App\Modules\Feedback\Sections\Components\FeedbackTranslate;
use App\Modules\Feedback\Sections\Custom\CallBackFeedBack;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;

class ModuleSeeder extends AbstractModuleSeeder
{

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            CallBackFeedBack::class => "Call Back Feedback",
            FeedbackGroupBlock::class => "Инфоблоки Feedback",
            FeedbackTranslate::class => "Переводы Feedback",
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "content_group",
                "title" => "Контент",
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "feedback_group",
                        "title" => "Обратная связь",
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "call_back_feedback",
                                "title" => "Call Back Feedback",
                                "section_class" => CallBackFeedBack::class
                            ]
                        ]
                    ],
                    [
                        "slug" => "block_groups",
                        "title" => "Инфоблоки",
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "feedback_blocks",
                                "title" => "Feedback",
                                "section_class" => FeedbackGroupBlock::class,
                                "order" => 6,
                            ]
                        ]
                    ]
                ]
            ],
            [
                "slug" => "configs",
                "title" => "Конфигурации",
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "translate_groups",
                        "title" => "Переводы",
                        "order" => 2,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "feedback_translates",
                                "title" => "Feedback",
                                "section_class" => FeedbackTranslate::class,
                                "order" => 6
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }
}
