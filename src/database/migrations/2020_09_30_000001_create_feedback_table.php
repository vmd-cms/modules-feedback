<?php

use VmdCms\Modules\Feedback\Models\Feedback as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang_key', 5)->nullable();
            $table->string('key', 64)->nullable();
            $table->string('resource', 128)->nullable();
            $table->integer('resource_id')->nullable()->unsigned();
            $table->string('name',128)->nullable();
            $table->string('email',128)->nullable();
            $table->string('phone',128)->nullable();
            $table->text('data')->nullable();
            $table->string('ip',128)->nullable();
            $table->boolean('checked')->default(false);
            $table->integer('checked_moderator_id')->nullable()->unsigned();
            $table->timestamp('checked_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
