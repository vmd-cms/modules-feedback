<?php

namespace VmdCms\Modules\Feedback\Initializers;;

use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventModelDTO;
use VmdCms\CoreCms\CoreModules\Events\Entity\CoreEventsSetup;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;
use VmdCms\Modules\Feedback\Services\CoreEventEnums;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'feedback';
    const ALIAS = 'Feedback';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishServices(true);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    public function seedCoreEvents()
    {
        CoreEventsSetup::getInstance()
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_FEEDBACK_CREATE))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->seed();
    }
}
