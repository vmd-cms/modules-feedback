<?php

namespace VmdCms\Modules\Feedback\Sections\Custom;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\Feedback\Sections\Feedback;

class CallBackFeedBack extends Feedback
{
    /**
     * @var string
     */
    protected $slug = 'call_back_feedback';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Call Back";
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('title','Title'),
            Column::text('active', 'Active'),
            Column::date('created_at', 'Created at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('title','Title')->required(),
            FormComponent::switch('active','Active'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Feedback\Models\Custom\CallBackFeedBack::class;
    }
}
