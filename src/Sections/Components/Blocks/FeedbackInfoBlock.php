<?php

namespace VmdCms\Modules\Feedback\Sections\Components\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\Feedback\Sections\Components\FeedbackGroupBlock;

class FeedbackInfoBlock extends FeedbackGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key','Ключ')->setDisabled(true),
            FormComponent::switch('active','Active'),
            FormComponent::input('description','Описание'),
            FormComponent::input('title','Title'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Feedback\Models\Components\Blocks\FeedbackInfoBlock::class;
    }

}

