<?php

namespace VmdCms\Modules\Feedback\Sections\Components;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Block;
use VmdCms\CoreCms\Exceptions\Sections\SectionCmsModelException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Services\CoreRouter;
use App\Modules\Feedback\Services\BlockEnum;
use App\Modules\Feedback\Services\BlockSectionFactory;

class FeedbackGroupBlock extends Block
{
    /**
     * @var string
     */
    protected $slug = 'feedback_blocks';

    /**
     * @return string
     */
    protected function getBaseCmsModelClass()
    {
        return \App\Modules\Feedback\Models\Components\FeedbackGroupBlock::class;
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return Form::panel([
            FormComponent::select('key','Ключ')->setEnumValues(BlockEnum::getEnums())->unique()->required(),
            FormComponent::switch('active','Active'),
            FormComponent::input('description','Описание')
        ]);
    }

    protected $inheritSection;

    /**
     * @return AdminSectionInterface
     * @throws SectionCmsModelException
     * @throws SectionNotFoundException
     */
    protected function getInheritSection()
    {
        if(Route::current()->getName() != CoreRouter::ROUTE_EDIT_GET &&
            Route::current()->getName() != CoreRouter::ROUTE_EDIT_PUT &&
            empty(request()->id))
        {
            throw new SectionCmsModelException();
        }

        if(!$this->inheritSection instanceof AdminSectionInterface)
        {
            $baseModelClass = $this->getBaseCmsModelClass();
            $model = $baseModelClass::find(request()->id);
            if(!$model instanceof \VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block) throw new ModelNotFoundException();
            $this->inheritSection =  (new BlockSectionFactory())->getBlockModel($model->key);
        }
        return $this->inheritSection;
    }
}
