<?php

namespace VmdCms\Modules\Feedback\Sections\Components;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class FeedbackTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'feedback_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Feedback\Models\Components\FeedbackTranslate::class;
    }
}
