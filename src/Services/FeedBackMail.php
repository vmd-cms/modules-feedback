<?php
namespace VmdCms\Modules\Feedback\Services;

use Illuminate\Support\Facades\Mail;
use VmdCms\CoreCms\Services\Mail\AbstractEmail;

class FeedBackMail extends AbstractEmail
{
    private $model;
    private $url;

    public function __construct(\VmdCms\Modules\Feedback\Models\Feedback $model)
    {
        $this->model = $model;
        $this->url = env('APP_URL') . '/admin/feedback/' . $this->model->id . '/edit';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))
            ->markdown('content::layouts.templates.mail.feedback')->subject('Новая заявка')
            ->with(['model' => $this->model, 'url' => $this->url]);
    }

    public function sendMail($email)
    {
        Mail::to($email)->send($this);
    }
}
