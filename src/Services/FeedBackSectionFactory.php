<?php

namespace VmdCms\Modules\Feedback\Services;

use VmdCms\Modules\Feedback\Models\Custom\CallBackFeedBack;

class FeedBackSectionFactory
{
    public static function getModelClass($key)
    {
        switch ($key){
            case CallBackFeedBack::getModelKey(): return \App\Modules\Feedback\Models\Custom\CallBackFeedBack::class;
            break;
            default: return null;
        }
    }
}
