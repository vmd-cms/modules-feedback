<?php

namespace VmdCms\Modules\Feedback\Services;

use VmdCms\CoreCms\Services\Enums;

class BlockEnum extends Enums
{
    const FEEDBACK_INFO_BLOCK = 'feedback_info_block';
}
