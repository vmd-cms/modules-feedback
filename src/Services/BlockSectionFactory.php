<?php

namespace VmdCms\Modules\Feedback\Services;

use VmdCms\CoreCms\Services\BlockSectionFactoryAbstract;
use App\Modules\Feedback\Models\Components\Blocks\FeedbackInfoBlock;

class BlockSectionFactory extends BlockSectionFactoryAbstract
{
    protected static function getAssocKeySectionClasses() : array
    {
        return [
            FeedbackInfoBlock::getModelKey() => \App\Modules\Feedback\Sections\Components\Blocks\FeedbackInfoBlock::class,
        ];
    }
}
