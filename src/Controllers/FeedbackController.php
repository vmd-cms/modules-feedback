<?php

namespace VmdCms\Modules\Feedback\Controllers;

use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\Modules\Feedback\Services\FeedBackSectionFactory;

class FeedbackController extends CoreController
{
    public function store(Request $request){
        $feedBackClass = FeedBackSectionFactory::getModelClass($request->get('key'));
        if(!$feedBackClass) return back()->withErrors();
        return (new $feedBackClass())->store($request);
    }
}
