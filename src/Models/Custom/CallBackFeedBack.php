<?php

namespace VmdCms\Modules\Feedback\Models\Custom;

use VmdCms\Modules\Feedback\Models\Feedback;

class CallBackFeedBack extends Feedback
{
    public static function getModelKey(){
        return "call_back";
    }
}
