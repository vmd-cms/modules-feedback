<?php

namespace VmdCms\Modules\Feedback\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\JsonData;
use App\Modules\Feedback\Services\FeedBackMail;
use VmdCms\Modules\Feedback\Services\CoreEventEnums;


class Feedback extends CmsModel
{
    use JsonData;

    public static function table(): string
    {
        return 'feedback';
    }

    public static function getModelKey(){
        return null;
    }

    public static function getFullActionPath(){
        return '/feedback/' . static::getModelKey();
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });

            static::saving(function (Feedback $model){
                $model->key = static::getModelKey();
            });
        }
    }

    protected function getValidateRequestRules()
    {
        return [
            'name' => 'max:128',
            'phone' => 'min:18|max:18',
            'email' => 'email|max:128',
        ];
    }

    public function store(Request $request){
        $request->validate($this->getValidateRequestRules());
        $this->name = $request->get('name');
        $this->phone = $request->get('phone');
        $this->email = $request->get('email');
        foreach ($this->getJsonDataFieldsArr() as $field)
        {
            $this->$field = $request->get($field);
        }
        $this->save();
        Event::dispatch(CoreEventEnums::USER_FEEDBACK_CREATE,[
            'user_email' => $this->email,
            'user_phone' => $this->phone,
            'admin_link' => request()->root() . '/dashboard/call_back_feedback/edit/' . $this->id
        ]);
        if ($request->ajax()) return json_encode(['status' => 'success']);
        else return back();
    }

    protected function getAdminEmail()
    {
        return 'svkuaod+feedback@gmail.com';
    }

    public function sendNotifications(){
        try{
            (new FeedBackMail($this))->sendMail($this->getAdminEmail());
        }
        catch (\Exception $e){
            Log::error($e->getMessage());
        }
    }
}
