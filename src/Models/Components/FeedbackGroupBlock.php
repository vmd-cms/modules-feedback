<?php

namespace VmdCms\Modules\Feedback\Models\Components;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class FeedbackGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'feedback';
    }
}
