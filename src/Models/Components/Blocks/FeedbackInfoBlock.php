<?php

namespace VmdCms\Modules\Feedback\Models\Components\Blocks;

use VmdCms\Modules\Feedback\Models\Components\FeedbackGroupBlock;
use VmdCms\Modules\Feedback\Services\BlockEnum;

class FeedbackInfoBlock extends FeedbackGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::FEEDBACK_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title'];
    }
}
