<?php

namespace VmdCms\Modules\Feedback\Models\Components;

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;

class FeedbackTranslate extends Translate
{
    public static function getModelGroup(): ?string
    {
        return 'feedback';
    }
}
